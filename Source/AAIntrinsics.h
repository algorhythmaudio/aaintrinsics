/*
 *	File:		AAIntrinsics.h
 *
 *	Version:	1.0.132
 *
 *	Created:	12-12-09 by Christian Floisand
 *	Updated:	13-01-16 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  Defines a set of functions that use Intel SSE SIMD intrinsics for very fast
 *  calculations.  These operations are most efficient when data is in blocks so
 *  that it can be packed into the 128-bit registers.
 *
 *	NOTE: Some intrinsics make use of the SSE4.1 extensions, which must be supported and 
 *	enabled.  To enable for compilation, set the -msse4.1 flag in "Other C flags" within 
 *	build settings (Xcode), or as a general compiler flag if compiling from the command prompt.
 *	NOTE: The 64-bit AVX intrinsics need to be enabled with the compiler flag -mavx.
 *
 *	Compiler defines:
 *		- AAINTRINSICS_USE_SSE_32bit : set to enable 32-bit SSE functions
 *		- AAINTRINSICS_USE_AVX_64bit : set to enable 64-bit AVX functions -> Deprecated for the time being; do not use!
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#ifndef _AAIntrinsics_h_
#define _AAIntrinsics_h_

// use these macros to align static memory allocation
#define		ALIGN_16	__declspec(align(16))	// 16-byte alignment, for SSE intrinsics
#define		ALIGN_32	__declspec(align(32))	// 32-byte alignment, for AVX intrinsics

#if defined AAINTRINSICS_USE_SSE_32bit || defined AAINTRINSICS_USE_AVX_64bit
#include <smmintrin.h>	// includes up to SSE4.1
#endif
#if defined AAINTRINSICS_USE_AVX_64bit
#include <immintrin.h>	// AVX
#endif

#include "AADsp.h"


namespace AADsp {
    
#pragma mark ____SSE functions
//////////////////////////////////
// SSE functions /////////////////
//////////////////////////////////
	
#ifdef AAINTRINSICS_USE_SSE_32bit
	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Intrinsics: Dynamic aligned memory allocation
//
// Memory allocated to be used with intrinsic functions should be byte-aligned
// (16-byte with 32-bit data, and 32-byte with 64-bit data). Memory allocated in
// Mac OS X is automatically byte-aligned, but not in Windows. Thus, any dynamically
// allocated memory need to use these functions to ensure proper data alignment
// in Windows.
//
// @Functions
//      - mallocAligned: returns the pointer to the 16-byte allocated buffer of size (in bytes)
//		- mallocFree: frees the aligned buffer allocated with mallocAligned
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void* aA_SSE_mallocAligned (const uint size);
void aA_SSE_freeAligned (void *buf);

	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Intrinsics: Basics
//
// @Functions
//      - calcRms: returns the RMS of the given audio signal buffer
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
//< Requires: SSE >
float aA_SSE_calcRms (const float *inBuffer,		// input audio buffer
					  const uint length);			//<! length of vector must be a multiple of 4

	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Intrinsics: Interpolation
//
// @Functions
//      - vInterpLinear: linearly interpolates the vector sourceY given the phase values in sourceX
//		- interpolateCubic: returns result of single-sample cubic interpolation
// @Notes
//		- interpolateCubic is not vectorized and so will not offer computational advantages
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
//< Requires: SSE4.1 >
void aA_SSE_vInterpLinear (const float *sourceX,	// vector of phase (x axis) values
						   const float *sourceY,	// vector of values (y axis) to interpolate between
						   float *resultY,			// resulting vector
						   const uint lengthX);		// length of vector containing phases (also equal to the length of resultY)
    
float aA_SSE_interpolateCubic (const float xPos,	// current position
							   const float *Y);		// array of four points, where xPos lies between Y[1] and Y[2]
    
	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Intrinsics: Vector operations
//
// @Functions
//      - vMax: returns the maximum (absolute) value contained in the vector
//		- vScale: multiplies contents of vector by the constant 'val' (scaling) and stores it in result
//		- vMultiply: multiplies two vectors of specified length together and stores it in result
//		- vAccumulate: adds all elements of a single vector together and returns the result
//		- vAdd: adds the elements of two vectors together
//		- vFillAdd: fills a vector by adding an increment value to each successive element, starting with an initial value
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//< Requires: SSE2 >
float aA_SSE_vMax (const float *inVector,			// input vector
				   const uint length);				//<! length of vector must be a multiple of 4

//< Requires: SSE >
void aA_SSE_vScale (const float *inVector,			// input vector
					const float scaleVal,			// scaling value
					float *outVector,				// output vector result; can be the same as inVector
					const uint length);				// length of both vectors
	
//< Requires: SSE >
void aA_SSE_vMultiply (const float *inVector1,		// input vector 1
					   const float *inVector2,		// input vector 2
					   float *outVector,			// output vector result; can be the same as either inVector 1 or 2
					   const uint length);			// length of vectors
	
//< Requires: SSE3 >
float aA_SSE_vAccumulate (const float *inVector,	// input vector
						  const uint length);		// length of vector
	
//< Requires: SSE >
void aA_SSE_vAdd (const float *inVector1,			// input vector 1
				  const float *inVector2,			// input vector 2
				  float *outVector,					// result of vector 1 + vector 2
				  const uint length);				// length of vectors
	
//< Requires: SSE >
void aA_SSE_vFillAdd (float *outVector,				// output vector to fill
					  const float initValue,		// initial value at vector index [0]
					  const float incr,				// increment amount
					  const uint length);			// length of vector
	
#endif
	
#pragma mark ____AVXfunctions
//////////////////////////////////
// AVX functions /////////////////
// See above for documentation ///
//////////////////////////////////

#ifdef AAINTRINSICS_USE_AVX_64bit
#if 0	// deprecated; do not use!
	
double aA_AVX_calcRms (const double *inBuffer,		// input audio buffer
					   const uint length);			//<! length of vector must be a multiple of 4
	
double aA_AVX_interpolateLinear (const double xPos,	// current position
								 const double *Y);	// array of the two points between which xPos lies
    
double aA_AVX_interpolateCubic (const double xPos,	// current position
								const double *Y);	// array of four points, where xPos lies between Y[1] and Y[2]
	
double aA_AVX_vMax (const double *inVector,			// input vector
					const uint length);				//<! length of vector must be a multiple of 4
	
void aA_AVX_vScale (const double *inVector,			// input vector
					const double val,				// scaling value
					double *outVector,				// output vector result; can be the same as inVector
					const uint length);				// length of both vectors
	
void aA_AVX_vMultiply (const double *inVector1,		// input vector 1
					   const double *inVector2,		// input vector 2
					   double *outVector,			// output vector result; can be the same as either inVector 1 or 2
					   const uint length);			// length of vectors
	
double aA_AVX_vAccumulate (const double *inVector,	// input vector
						   const uint length);		// length of vector
	
#endif
#endif
    
} // AADsp namespace

#endif // _AAIntrinsics_h_
