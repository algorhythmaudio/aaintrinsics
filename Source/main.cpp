//
//  main.cpp
//  AAIntrinsics tests and implementation guide
//
//  Created by Christian on 2013-01-26.
//  Copyright (c) 2013 Algorhythm Audio. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <algorithm>

// AA libraries
#include "AADsp.h"
#include "AABasic.h"
#include "AATimer.h"
#include "AAIntrinsics.h"

#define kBufferSize 8192

int main(int argc, const char * argv[])
{
    using namespace AADsp;
    
    AATimer timeClock;
    
    float result = 0.f;
    float *buffer = (float*)aA_SSE_mallocAligned(sizeof(float)*kBufferSize);
    
    buffer[0] = 0.f;
    for (int i = 1; i < kBufferSize; ++i)
        buffer[i] = buffer[i-1] + 0.1;
    
    timeClock.startTime();
    result = aA_SSE_calcRms(buffer, kBufferSize);
    timeClock.stopTime();
    
    std::cout << "RMS: " << result << std::endl;
    timeClock.printTimeElapsedMessage("aA_SSE_calcRms");
    aA_SSE_freeAligned(buffer);
    
    ALIGN_16 float vec[100000];
    float vmax = 0.f;
    std::fill(vec, vec+100000, 3.f);
    
    timeClock.startTime();
    vmax = aA_SSE_vMax(vec, 100000);   // find maximum (absolute) value in vec
    timeClock.stopTime();
    
    std::cout << "Max in vector: " << vmax << std::endl;
    timeClock.printTimeElapsedMessage("aA_SSE_vMax");
    
    ALIGN_16 float vec2[8] = {2.f, 2.f, 2.f, 4.f, 4.f, 6.f, 6.f, 9.f};
    ALIGN_16 float ans[8];
    
    timeClock.startTime();
    aA_SSE_vScale(vec2, 2.f, ans, 8);  // multiply contents of vec2 by 2, and store result in ans
    for (int i = 0; i < 8; ++i)
        std::cout << ans[i] << " ";
    std::cout << std::endl;
    aA_SSE_vScale(vec2, 3.f, vec2, 8);  // multiply contents of vec2 by 2, and store results in vec2
    for (int i = 0; i < 8; ++i)
        std::cout << vec2[i] << " ";
    std::cout << std::endl;
    timeClock.stopTime();
    timeClock.printTimeElapsedMessage("aA_SSE_vScale");
    
    ALIGN_16 float vecMul1[10];
    ALIGN_16 float vecMul2[10];
    ALIGN_16 float vecMulRes[10];
    std::fill(vecMul1, vecMul1+8, 2.f);
    std::fill(vecMul2, vecMul2+8, 2.f);
    vecMul1[8] = 12.f; vecMul1[9] = 8.f;
    vecMul2[8] = 4.f; vecMul2[9] = 8.f;
    
    timeClock.startTime();
    aA_SSE_vMultiply(vecMul1, vecMul2, vecMulRes, 10);
    timeClock.stopTime();
    
    std::cout << "Vector multiplication: ";
    for (int i = 0; i < 10; ++i)
        std::cout << vecMulRes[i] << " ";
    std::cout << std::endl;
    timeClock.printTimeElapsedMessage("aA_SSE_vMultiply");
    
    ALIGN_16 float vecAcc[32];
    float accRes = 0.f;
    for (int i = 0; i < 32; ++i)
        vecAcc[i] = i;
    
    timeClock.startTime();
    accRes = aA_SSE_vAccumulate(vecAcc, 32);
    timeClock.stopTime();
    
    std::cout << "Vector accumulation: " << accRes << std::endl;
    timeClock.printTimeElapsedMessage("aA_SSE_vAccumulate");
    
    float table[6] = { 0.f, 3.f, 3.8f, 6.1f, 7.33f, 10.f };
    float phase[10000];
    float lineresult[10000];
    
    phase[0] = 0.f;
    for (int p = 1; p < 10000; ++p)
        phase[p] = phase[p-1] + 5e-4;
    assert(phase[9999] < 5.f);
    
    timeClock.startTime();
    aA_SSE_vInterpLinear(phase, table, lineresult, 10000);
    timeClock.stopTime();
    timeClock.printTimeElapsedMessage("aA_SSE_vInterpLinear");
    
    std::ofstream fout;
    
    fout.open("/Users/Rainland/Desktop/interp.txt");
    for (int i = 0; i < 10000; ++i)
        fout << phase[i] << "\t" << lineresult[i] << "\n";
    fout.close();
    
    float fillVector[10000];
    
    timeClock.startTime();
    aA_SSE_vFillAdd(fillVector, 0.f, 0.3f, 10000);
    timeClock.stopTime();
    timeClock.printTimeElapsedMessage("aA_SSE_vFillAdd");
    
    float addVec1[10] = { 2.f, 2.f, 3.f, 3.f, 1.f, 6.f, 10.f, 40.f, 100.f, 100.f };
    float addVec2[10] = { 10.5f, 10.f, 10.f, 1.f, 1.f, 1.f, 2.f, 2.f, 50.f, 20.f };
    float addVecRes[10];
    
    aA_SSE_vAdd(addVec1, addVec2, addVecRes, 10);
    
    for (int i = 0; i < 10; ++i)
        std::cout << addVecRes[i] << " ";
    std::cout << std::endl;
    
    return 0;
}


