/*
 *	File:		AAIntrinsics.h
 *
 *	Version:	1.0
 *
 *	Created:	12-12-14 by Christian Floisand
 *	Updated:	13-01-15 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  Function definitions for AA SIMD intrinsics.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#include "AAIntrinsics.h"
#include "AABasic.h"


namespace AADsp {

	
#pragma mark ____Basics x86
	
#ifdef AAINTRINSICS_USE_SSE_32bit
	
// aA_SSE_mallocAligned; allocates 16-byte aligned memory block
//-----------------------------------------------------------------------------------
void* aA_SSE_mallocAligned (const uint size)
{
#if defined __APPLE__ && defined __MACH__
	return _mm_malloc(size, 16);
#elif defined _WIN32 || defined _WIN64
	return _aligned_malloc(size, 16);
#else
	return NULL;
#endif
}
	
// aA_SSE_freeAligned; frees 16-byte aligned memory block
//-----------------------------------------------------------------------------------
void aA_SSE_freeAligned (void *buf)
{
#if defined __APPLE__ && defined __MACH__
	_mm_free(buf);
#elif defined _WIN32 || defined _WIN64
	_aligned_free(buf);
#else
	return;
#endif
}

// aA_SSE_calcRms, 32-bit -- Required: SSE
//-----------------------------------------------------------------------------------
float aA_SSE_calcRms (const float *inBuffer, const uint length)
{
    assert(length % 4 == 0);
    
    __m128 *mbuf = (__m128*)inBuffer;
    __m128 m1, m2;
    __m128 macc = _mm_set1_ps(0.f);
    __m128 _1oN = _mm_set1_ps(1.f/static_cast<float>(length));
    
    for (int i = 0; i < length; i+=4) {
        m1 = _mm_mul_ps(*mbuf, *mbuf);
        m2 = _mm_mul_ps(m1, _1oN);
        macc = _mm_add_ps(m2, macc);
        mbuf++;
    }
    
    ALIGN_16 float result[4];
    _mm_store_ps(result, macc);
    
    return sqrt(result[0] + result[1] + result[2] + result[3]);
}


#pragma mark ____Interpolation x86

// aA_SSE_vInterpLinear, 32-bit -- Required: SSE4.1
//-----------------------------------------------------------------------------------
void aA_SSE_vInterpLinear (const float *sourceX, const float *sourceY, float *resultY, const uint lengthX)
{
	assert(lengthX % 4 == 0);
	
	__m128 *mXphases = (__m128*)sourceX;
	__m128 *mYresult = (__m128*)resultY;
	__m128 mX0, mY0, mY1, mYtemp, mXtemp, mtemp;
	ALIGN_16 float pos[4];
	
	for (int i = 0; i < lengthX; i+=4) {
		// floor the values in X to get indexing positions at X0, and add 1 for X1 index positions
		mX0 = _mm_floor_ps(*mXphases);
		_mm_store_ps(pos, mX0);
		mY0 = _mm_set_ps(sourceY[(int)pos[3]], sourceY[(int)pos[2]], sourceY[(int)pos[1]], sourceY[(int)pos[0]]);
		mY1 = _mm_set_ps(sourceY[(int)pos[3]+1], sourceY[(int)pos[2]+1], sourceY[(int)pos[1]+1], sourceY[(int)pos[0]+1]);
		
		mYtemp = _mm_sub_ps(mY1, mY0);
		mXtemp = _mm_sub_ps(*mXphases, mX0);
		mtemp = _mm_mul_ps(mYtemp, mXtemp);
		*mYresult = _mm_add_ps(mY0, mtemp);
		
		mXphases++;
		mYresult++;
	}
}

// aA_SSE_interpolateCubic, 32-bit
//-----------------------------------------------------------------------------------
float aA_SSE_interpolateCubic (const float xPos, const float *Y)
{
	ALIGN_16 float result1[4];
	ALIGN_16 float result2[4];
	
	// this function is slow because of register spilling
	__m128 mCoeffsA = _mm_set_ps(0.5f, -0.5f, -1.5f, 1.5f);
	__m128 mregA = _mm_set_ps(Y[3], Y[0], Y[2], Y[1]);
	__m128 mCoeffsB = _mm_set_ps(-0.5f, -2.5f, 1.f, 2.f);
	__m128 mregB = _mm_set_ps(Y[3], Y[1], Y[0], Y[2]);
	__m128 mCoeffsC = _mm_set_ps(0.f, 0.f, 0.5f, 0.5f);
	__m128 mregC = _mm_set_ps(0.f, 0.f, Y[0],Y[2]);
	
	__m128 mA = _mm_mul_ps(mCoeffsA, mregA);
	__m128 mB = _mm_mul_ps(mCoeffsB, mregB);
	__m128 mC = _mm_mul_ps(mCoeffsC, mregC);
	__m128 mAB = _mm_hadd_ps(mA, mB);
	
	_mm_store_ps(result1, mAB);
	_mm_store_ps(result2, mC);
	
	float frac = xPos - floor(xPos);
	float a = result1[0] + result1[1];
	float b = result1[2] + result1[3];
	float c = result2[0] - result2[1];
	
	return (((a * frac) + b) * frac + c) * frac + Y[1];
}


#pragma mark ____Vector operations x86

// aA_SSE_vMax, 32-bit -- Required: SSE2
//-----------------------------------------------------------------------------------
float aA_SSE_vMax (const float *inVector, const uint length)
{
	assert(length % 4 == 0);
	
	static const __m128 SIGNMASK = _mm_castsi128_ps(_mm_set1_epi32(0x80000000));
	__m128 *mvec = (__m128*)inVector;
	__m128 mmax = _mm_set1_ps(0.f);
	__m128 mabs;
	
	for (int i = 0; i < length; i+=4) {
		mabs = _mm_andnot_ps(SIGNMASK, *mvec);	// absolute value of data in *mvec
		mmax = _mm_max_ps(mabs, mmax);
		mvec++;
	}
	
	ALIGN_16 float result[4];
	_mm_store_ps(result, mmax);
	
	return aA_max<float>( aA_max<float>(result[0], result[1]), aA_max<float>(result[2], result[3]) );
}

// aA_SSE_vScale, 32-bit -- Required: SSE
//-----------------------------------------------------------------------------------
void aA_SSE_vScale (const float *inVector, const float scaleVal, float *outVector, const uint length)
{
	__m128 *mvec = (__m128*)inVector;
	__m128 *mres = (__m128*)outVector;
	__m128 mval = _mm_set1_ps(scaleVal);
	
	for (int i = 0; i < (length>>2); ++i) {
		*mres = _mm_mul_ps(*mvec, mval);
		mvec++;
		mres++;
	}
	
	const uint remainder = length % 4;
	if (remainder) {
		for (int i = remainder; i > 0; --i) {
			outVector[length-i] = inVector[length-i] * scaleVal;
		}
	}
}

// aA_SSE_vMultiply, 32-bit -- Required: SSE
//-----------------------------------------------------------------------------------
void aA_SSE_vMultiply (const float *inVector1, const float *inVector2, float *outVector, const uint length)
{
	__m128 *mvec1 = (__m128*)inVector1;
	__m128 *mvec2 = (__m128*)inVector2;
	__m128 *mres = (__m128*)outVector;
	
	for (int i = 0; i < (length>>2); ++i) {
		*mres = _mm_mul_ps(*mvec1, *mvec2);
		mvec1++;
		mvec2++;
		mres++;
	}
	
	const uint remainder = length % 4;
	if (remainder) {
		for (int i = remainder; i > 0; --i) {
			outVector[length-i] = inVector1[length-i] * inVector2[length-i];
		}
	}
}

// aA_SSE_vAccumulate, 32-bit -- Required: SSE3
//-----------------------------------------------------------------------------------
float aA_SSE_vAccumulate (const float *inVector, const uint length)
{
	__m128 *mvec = (__m128*)inVector;
	__m128 macc = _mm_set1_ps(0.f);
	__m128 m1, m2;
	
	for (int i = 0; i < (length>>2); ++i) {
		m1 = _mm_hadd_ps(*mvec, *mvec);
		m2 = _mm_hadd_ps(m1, m1);		// horizontal add previous, resulting in a0+a1+a2+a3
		macc = _mm_add_ps(m2, macc);
		mvec++;
	}
	
	ALIGN_16 float result[4];
	_mm_store_ss(result, macc);
	
	const uint remainder = length % 4;
	float remainder_acc = 0.f;
	if (remainder) {
		for (int i = remainder; i > 0; --i) {
			remainder_acc += inVector[length-i];
		}
	}
	
	return result[0] + remainder_acc;
}
	
// aA_SSE_vAdd, 32-bit -- Required: SSE
//-----------------------------------------------------------------------------------
void aA_SSE_vAdd (const float *inVector1, const float *inVector2, float *outVector, const uint length)
{
	__m128 *mvec1 = (__m128*)inVector1;
	__m128 *mvec2 = (__m128*)inVector2;
	__m128 *mres = (__m128*)outVector;
	
	for (int i = 0; i < (length>>2); ++i) {
		*mres = _mm_add_ps(*mvec1, *mvec2);
		mvec1++;
		mvec2++;
		mres++;
	}
	
	const uint remainder = length % 4;
	if (remainder) {
		for (int i = remainder; i > 0; --i) {
			outVector[length-i] = inVector1[length-i] + inVector2[length-i];
		}
	}
}
	
// aA_SSE_vFillAdd, 32-bit -- Required: SSE
//-----------------------------------------------------------------------------------
void aA_SSE_vFillAdd (float *outVector, const float initValue, const float incr, const uint length)
{
	__m128 *mres = (__m128*)outVector;
	__m128 *mpre = mres;
	__m128 mincr = _mm_set1_ps(incr*4.f);
	
	*mres++ = _mm_set_ps(initValue+incr*3.f, initValue+incr*2.f, initValue+incr, initValue);
	for (int i = 4; i < length; i+=4) {
		*mres = _mm_add_ps(*mpre, mincr);
		mpre = mres;
		mres++;
	}
	
	const uint remainder = length % 4;
	if (remainder) {
		for (int i = remainder; i > 0; --i) {
			outVector[length-i] = outVector[length-i-1] + incr;
		}
	}
}


#endif
	
	
#pragma mark ____Basics x64
	
#ifdef AAINTRINSICS_USE_AVX_64bit
#if 0
	
// aA_AVX_calcRms, 64-bit
//-----------------------------------------------------------------------------------
double aA_AVX_calcRms (const double *inBuffer, const uint length)
{
	assert(length % 4 == 0);
	
	__m256d *mbuf = (__m256d*)inBuffer;
	__m256d m1, m2;
	__m256d macc = _mm256_set1_pd(0.l);		// initialize to 0
	__m256d _1oN = _mm256_set1_pd(1./static_cast<double>(length));
	
	for (int i = 0; i < length; i+=4) {
		m1 = _mm256_mul_pd(*mbuf, *mbuf);  // buf[i] ^ 2
		m2 = _mm256_mul_pd(m1, _1oN);      // * 1/n
		macc = _mm256_add_pd(m2, macc);    // accumulate
		mbuf++;
	}
	
	ALIGN_32 double result[4];
	_mm256_store_pd(result, macc);
	
	return sqrt(result[0] + result[1] + result[2] + result[3]);
}
	
	
#pragma mark ____Interpolation x64
	
// aA_AVX_interpolateLinear, 64-bit
//-----------------------------------------------------------------------------------
double aA_AVX_interpolateLinear (const double xPos, const double *Y)
{
	ALIGN_32 double result[4];
	const uint pos = static_cast<uint>(xPos);
	
	__m256d mregX = _mm256_set_pd(xPos, floorf(xPos), xPos, floorf(xPos));
	__m256d mregY = _mm256_set_pd(Y[pos+1], Y[pos+1], Y[pos], Y[pos]);
	__m256d mXY = _mm256_mul_pd(mregX, mregY);
	
	_mm256_store_pd(result, mXY);
	
	return Y[pos] + (result[0] - result[1] - result[2] + result[3]);
}
	
// aA_AVX_interpolateCubic, 64-bit
//-----------------------------------------------------------------------------------
double aA_AVX_interpolateCubic (const double xPos, const double *Y)
{
	ALIGN_32 double result1[4];
	ALIGN_32 double result2[4];
	
	__m256d mCoeffsA = _mm256_set_pd(0.5l, -0.5l, -1.5l, 1.5l);
	__m256d mregA = _mm256_set_pd(Y[3], Y[0], Y[2], Y[1]);
	__m256d mCoeffsB = _mm256_set_pd(-0.5l, -2.5l, 1.l, 2.l);
	__m256d mregB = _mm256_set_pd(Y[3], Y[1], Y[0], Y[2]);
	__m256d mCoeffsC = _mm256_set_pd(0.l, 0.l, 0.5l, 0.5l);
	__m256d mregC = _mm256_set_pd(0.f, 0.f, Y[0],Y[2]);
	
	__m256d mA = _mm256_mul_pd(mCoeffsA, mregA);
	__m256d mB = _mm256_mul_pd(mCoeffsB, mregB);
	__m256d mC = _mm256_mul_pd(mCoeffsC, mregC);
	__m256d mAB = _mm256_hadd_pd(mA, mB);
	
	_mm256_store_pd(result1, mAB);
	_mm256_store_pd(result2, mC);
	
	double frac = xPos - floor(xPos);
	double a = result1[0] + result1[1];
	double b = result1[2] + result1[3];
	double c = result2[0] - result2[1];
	
	return (((a * frac) + b) * frac + c) * frac + Y[1];
}
	
	
#pragma mark ____Vector operations x64
	
// aA_AVX_vMax, 64-bit
//-----------------------------------------------------------------------------------
double aA_AVX_vMax (const double *inVector, const uint length)
{
	assert(length % 4 == 0);
	
	static const __m256 SIGNMASK = _mm256_castsi256_pd(_mm256_set1_epi32(0x80000000));
	__m256d *mvec = (__m256d*)inVector;
	__m256d mmax = _mm256_set1_pd(0.l);					// initialize to 0
	__m256d mabs;
	
	for (int i = 0; i < length; i+=4) {
		mabs = _mm256_andnot_pd(SIGNMASK, *mvec);		// take absolute value
		mmax = _mm256_max_pd(mabs, mmax);				// sore max value
		mvec++;
	}
	
	ALIGN_32 double result[4];
	_mm256_store_pd(result, mmax);
	
	// return the maximum value of the 4 largest values found in inVector
	return aAmax<double>( aAmax<double>(result[0], result[1]), aAmax<double>(result[2], result[3]) );
}
	
// aA_AVX_vScale, 64-bit
//-----------------------------------------------------------------------------------
void aA_AVX_vScale (const double *inVector, const double val, double *outVector, const uint length)
{
	__m256d *mvec = (__m256d*)inVector;
	__m256d *mres = (__m256d*)outVector;
	__m256d mval = _mm256_set1_pd(val);		// set scalar value to each position in the register
	__m256d mrem;
	
	for (int i = 0; i < (length>>2); ++i) {
		*mres = _mm256_mul_pd(*mvec, mval);	// multiply vector elements by scalar value
		mvec++;
		mres++;
	}
	
	// handle remainder if length is not a multiple of 4
	// set mrem to the last elements of the input vector and then multiply
	switch (length % 4) {
		case 0:
			return;
			break;
		case 1:
			mrem = _mm256_set_pd(0.l, 0.l, 0.l, inVector[length-1]);
			break;
		case 2:
			mrem = _mm256_set_pd(0.l, 0.l, inVector[length-2], inVector[length-1]);
			break;
		case 3:
			mrem = _mm256_set_pd(0.l, inVector[length-3], inVector[length-2], inVector[length-1]);
	}
	
	mres++;
	*mres = _mm256_mul_pd(mrem, mval);
}
	
// aA_AVX_vMultiply, 64-bit
//-----------------------------------------------------------------------------------
void aA_AVX_vMultiply (const double *inVector1, const double *inVector2, double *outVector, const uint length)
{
	__m256d *mvec1 = (__m256d*)inVector1;
	__m256d *mvec2 = (__m256d*)inVector2;
	__m256d *mres = (__m256d*)outVector;
	__m256d mrem1, mrem2;
	
	for (int i = 0; i < (length>>2); ++i) {
		*mres = _mm256_mul_pd(*mvec1, *mvec2);	// multiply vector elements together and store in address of result
		mvec1++;
		mvec2++;
		mres++;
	}
	
	// handle remainder if length is not a multiple of 4
	// set mrem registers to the last elements of the input vectors and then multiply
	switch (length % 4) {
		case 0:
			return;
			break;
		case 1:
			mrem1 = _mm256_set_pd(0.l, 0.l, 0.l, inVector1[length-1]);
			mrem2 = _mm256_set_pd(0.l, 0.l, 0.l, inVector2[length-1]);
			break;
		case 2:
			mrem1 = _mm256_set_pd(0.l, 0.l, inVector1[length-2], inVector1[length-1]);
			mrem2 = _mm256_set_pd(0.l, 0.l, inVector2[length-2], inVector2[length-1]);
			break;
		case 3:
			mrem1 = _mm256_set_pd(0.l, inVector1[length-3], inVector1[length-2], inVector1[length-1]);
			mrem2 = _mm256_set_pd(0.l, inVector2[length-3], inVector2[length-2], inVector2[length-1]);
	}
	
	mres++;
	*mres = _mm256_mul_pd(mrem1, mrem2);
}
	
// aA_AVX_vAccumulate, 64-bit
//-----------------------------------------------------------------------------------
double aA_AVX_vAccumulate (const double *inVector, const uint length)
{
	__m256d *mvec = (__m256d*)inVector;
	__m256d macc = _mm256_set1_pd(0.l);	// initialize accumulator to 0
	__m256d m1, m2;
	
	for (int i = 0; i < (length>>2); ++i) {
		m1 = _mm256_hadd_pd(*mvec, *mvec);	// horizontal add first two elements of the register
		m2 = _mm256_hadd_pd(m1, m1);		// horizontal add previous, resulting in a0+a1+a2+a3
		macc = _mm256_add_pd(m2, macc);		// accumulate
		mvec++;
	}
	
	double remainder = 0.l;
	ALIGN_32 double result[4];
	_mm256_store_pd(result, macc);
	
	// handle remainder if length is not a multiple of 4
	// set remainder to accumulate the last elements of the input vectors and then add
	for (int i = 0; i < (length%4); ++i)
		remainder += inVector[length-i-1];
	
	return result[0] + remainder;
}
	
#endif
#endif
	
} // AADsp namespace
